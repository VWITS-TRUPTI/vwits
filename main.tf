
# Specifying the AWS details, we can also pass version and keys

provider "aws" {

  region     = "eu-west-2"
 

}

# creating separate file for variables is best practice.

variable "subnet_prefix" {
  description = "cidr block for the subnet"

}

# create vpc
resource "aws_vpc" "prod-vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "production"
  }
}

# create a subnet (types of subnets in vpc - private subnet, public subnet and vpn only)
resource "aws_subnet" "subnet-1" {
  vpc_id            = aws_vpc.prod-vpc.id
  cidr_block        = var.subnet_prefix[0].cidr_block
  availability_zone = "eu-west-2b"

  tags = {
    Name = var.subnet_prefix[0].name
  }
}

resource "aws_subnet" "subnet-2" {
  vpc_id            = aws_vpc.prod-vpc.id
  cidr_block        = var.subnet_prefix[1].cidr_block
  availability_zone = "eu-west-2b"

  tags = {
    Name = var.subnet_prefix[1].name
  }
}

#Add security Group for Inbound Traffic

resource "aws_security_group" "allow_web" {
   name        = "allow_web_traffic"
   description = "Allow Web inbound traffic"
   vpc_id      = aws_vpc.prod-vpc.id

   ingress {
     description = "HTTPS"
     from_port   = 443
     to_port     = 443
     protocol    = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
   }
   ingress {
     description = "HTTP"
     from_port   = 80
     to_port     = 80
     protocol    = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
   }
   ingress {
     description = "SSH"
     from_port   = 22
     to_port     = 22
     protocol    = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
   }

   egress {
     from_port   = 0
     to_port     = 0
     protocol    = "-1"
     cidr_blocks = ["0.0.0.0/0"]
   }

   tags = {
     Name = "allow_web"
   }
 }


#Create EC2 instance - Ubuntu server and install/enable apache2

resource "aws_instance" "web-server-instance" {
   ami           = "ami-0fb391cce7a602d1f"
   instance_type = "t2.micro"
   availability_zone = "eu-west-2b"
  
}
